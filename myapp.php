<?php
use \App\DB\test\Test AS TestClass;
$student1 = new \App\User();
echo $student1->sayHello();
echo "<br>";
$db = new \App\DB\DataBase();
$db->test();
echo "<br>";
echo TestClass::test();

//autoload function
function __autoload($className){
    $class = 'Classes/'.str_replace('\\', '/', $className).'.php';
    require_once ($class);
}