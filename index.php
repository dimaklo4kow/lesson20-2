<?php
require_once "lib1.php";
require_once "User.php";
require_once "Student.php";
use MyProject\subLib\subLib2;
use App\users;
use App\users\Student AS app_student;

const MYCONST = 'index:MYCONST';
function myFunction(){
    return __FUNCTION__;
}

class MyClass{
    static function WhoAmI(){
        return __METHOD__;
    }
}

echo MyClass::WhoAmI();
echo "<br>";
echo subLib2\MyClass::WhoAmI();
echo MyClass::WhoAmI();
echo "<br>";
echo users\User::sayHello();
echo "<br>";
echo app_student::sayHello();